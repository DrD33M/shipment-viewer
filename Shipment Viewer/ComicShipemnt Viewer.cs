﻿using System;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Data.SQLite;

namespace Hitlist_Viewer
{
    public partial class Form1 : Form
    {


        private SQLiteConnection m_dbConnection;
        public Form1()
        {
            InitializeComponent();
            run();
        }

        async void run()
        {
            m_dbConnection = new SQLiteConnection("Data Source=Hitlist.sqlite;Version=3;");
            m_dbConnection.Open();
            DateTime nextWednesday = DateTime.Now.AddDays(1);
            while (nextWednesday.DayOfWeek != DayOfWeek.Wednesday)
                nextWednesday = nextWednesday.AddDays(1);
            DateTime lastWednesday = DateTime.Now.AddDays(-1);
            while (lastWednesday.DayOfWeek != DayOfWeek.Wednesday)
                lastWednesday = lastWednesday.AddDays(-1);
            JArray hitlist = await getHitlistAsync(string.Format("{0:M/d/yyyy}", nextWednesday)).ConfigureAwait(true);
            if (hitlist.Count > 5 && CreatTable(string.Format("{0:M/d/yyyy}", nextWednesday)) != false)
            {
                CreatTable(string.Format("{0:M/d/yyyy}", nextWednesday));
                SaveToDb(await getHitlistAsync(string.Format("{0:M/d/yyyy}", nextWednesday)).ConfigureAwait(true), string.Format("{0:M/d/yyyy}", nextWednesday));
            }
            
            if (CreatTable(string.Format("{0:M/d/yyyy}", lastWednesday)))
            {
                SaveToDb(await getHitlistAsync(string.Format("{0:M/d/yyyy}", lastWednesday)).ConfigureAwait(true), string.Format("{0:M/d/yyyy}", lastWednesday));
            }
            printList(string.Format("{0:M/d/yyyy}", nextWednesday));
            updateDropDown();

        }

        private void printList(string db)
        {
            string _get_db = string.Format("SELECT * FROM \"{0}\"", db);
            var dataAdapter = new SQLiteDataAdapter(_get_db, m_dbConnection);
            SQLiteCommandBuilder get_db = new SQLiteCommandBuilder(dataAdapter);
            var ds = new DataSet();
            dataAdapter.Fill(ds);

            dataGridView1.ReadOnly = true;
            dataGridView1.DataSource = ds.Tables[0];
            dataGridView1.Columns["id"].Visible = false;
            dataGridView1.Columns["publisher"].HeaderText = "Publisher";
            dataGridView1.Columns["title"].HeaderText = "Title";
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 15);
            dataGridView1.DefaultCellStyle.ForeColor = Color.Yellow;
            dataGridView1.DefaultCellStyle.BackColor = Color.Black ;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.Blue;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.CadetBlue;
        }

        private void updateDropDown()
        {
            string get_dbs = "SELECT * FROM sqlite_master where type='table'";
            comboBox1.Items.Clear();
            using (SQLiteCommand cmd = new SQLiteCommand(get_dbs, m_dbConnection))
            {
                using (SQLiteDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        if (rdr.GetString(1) != "sqlite_sequence")
                        {
                            comboBox1.Items.Add(rdr.GetString(1));
                        }
                    }
                }
            }
            comboBox1.SelectedIndex = comboBox1.Items.Count -1 ;
        }
        private async Task<JArray> getHitlistAsync(string date)
        {
            return await Task.Run(() => getHit(date));
           
        }

        private JArray getHit(string date)
        {
            JArray list = new JArray();
            WebClient client = new WebClient();

            string data = client.DownloadString(string.Format("https://leagueofcomicgeeks.com/comic/get_comics?list=releases&date_type=week&date={0}&date_end=&series_id=&user_id=0&title=&view=json&format[]=1", date));
            JObject json = JObject.Parse(data);

            string comics = json["list"].ToString();

            Regex.Replace(comics, " \r\n", "");
            Regex.Replace(comics, ">\\s+<", "><");

            var comic = new Regex("<li id=([\\s\\S]+?)<\\/li>");
            var comic_pre = new Regex("(?<=title\">).*?(?=</a>)");
            var comic_post = new Regex("(?<=\">)(.*)");
            var publisher = new Regex("(?<=ng>).*(?=</strong> &nbsp;&#183)");
            MatchCollection mc = comic.Matches(comics);
            foreach (Match item in mc)
            {
                Match _comic_name_pre = comic_pre.Match(item.ToString());
                Match _publisher = publisher.Match(item.ToString());
                Match _comic_name_post = comic_post.Match(_comic_name_pre.ToString());

                JObject _comic = new JObject(
                               new JProperty("title", _comic_name_post.ToString()),
                               new JProperty("publisher", _publisher.ToString()));
                list.Add(_comic);
            }
            return list;
        }

        private bool CreatTable(string date)
        {
            string check = string.Format("SELECT name FROM sqlite_master WHERE type='table' AND name='{0}'", date);
            string sql = string.Format("CREATE TABLE \"{0}\" ( id INTEGER PRIMARY KEY AUTOINCREMENT, title VARCHAR(20), publisher VARCHAR(20))", date);

            SQLiteCommand check_t = new SQLiteCommand(check, m_dbConnection);
            SQLiteCommand create_t = new SQLiteCommand(sql, m_dbConnection);

            object table = check_t.ExecuteScalar();
            if(table == null)
            {
                create_t.ExecuteNonQuery();
                return true;
            }

            return false;
            
        }

        private void SaveToDb(JArray shipment, string db)
        {
            foreach (JObject comic in shipment)
            {
                    string insert = string.Format("insert into \"{0}\" (title, publisher) values (@title, @publisher)", db);

                    SQLiteCommand check_q = new SQLiteCommand(insert, m_dbConnection);

                    check_q.Parameters.Add(new SQLiteParameter("@title", comic["title"]));
                    check_q.Parameters.Add(new SQLiteParameter("@publisher", comic["publisher"]));

                    check_q.ExecuteNonQuery();
            }

        }
        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = dataGridView1.DataSource;
            bs.Filter = "title like '%" + textBox1.Text + "%'";
            dataGridView1.DataSource = bs;
        }

        async private void Button1_Click(object sender, EventArgs e)
        {
            string date = comboBox1.SelectedItem.ToString();
            string insert = string.Format("DELETE FROM \"{0}\"", date);
            SQLiteCommand clear_db = new SQLiteCommand(insert, m_dbConnection);
            clear_db.ExecuteNonQuery();

            SaveToDb(await getHitlistAsync(date).ConfigureAwait(true), date);


        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string date = (string)comboBox1.SelectedItem;
            printList(date);
        }
    }
}
